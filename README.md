# fsb-mini-project

Game Service

## Getting started

Game Service handles all game-related operations (CRUD).

## Operations

GET /api/games  - list all games <br/>
GET /api/games/{name}  -  get game by name <br/>
POST /api/games  -  create new game <br/>
PUT /api/games  -  update existing game <br/>
DELETE /api/games/{name}  -  delete existing game <br/>


## Running Locally
Run the Spring Boot Application locally. <br/>
Use Swagger to try APIs - http://localhost:8080/swagger-ui.html 

