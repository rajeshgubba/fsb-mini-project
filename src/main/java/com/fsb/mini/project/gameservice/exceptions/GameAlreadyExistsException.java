package com.fsb.mini.project.gameservice.exceptions;

/**
 * Please add your description here.
 *
 */
public class GameAlreadyExistsException extends RuntimeException
{
    private static final long serialVersionUID = -7549512813770086915L;

    public GameAlreadyExistsException(String message) {
        super(message);
    }
}
