package com.fsb.mini.project.gameservice.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsb.mini.project.gameservice.model.Game;
import com.fsb.mini.project.gameservice.service.GameService;

/**
 * Controller for game requests.
 *
 */
@RestController
@RequestMapping("/api")
public class GameController
{

    private GameService gameService;

    public GameController( GameService gameService ) {
        this.gameService = gameService;
    }

    @GetMapping("/games")
    public ResponseEntity<List<Game>> list() {
        List<Game> games = gameService.listGames();
        if(games.isEmpty()) {
            return new ResponseEntity<>( HttpStatus.NO_CONTENT );
        }
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @GetMapping("/games/{name}")
    public ResponseEntity<Game> getByName(@PathVariable final String name ) {
        Game game =  gameService.findByName(name);
        return new ResponseEntity<>( game, HttpStatus.OK );
    }

    @PostMapping("/games")
    public ResponseEntity<String> createGame(@RequestBody final Game game) {
        gameService.createGame(game);
        return new ResponseEntity<>("Successfully created!", HttpStatus.CREATED);
    }

    @PutMapping("/games")
    public ResponseEntity<Game> updateGame(@RequestBody final Game game) {
        Game oldGame = gameService.updateGame(game);
        return new ResponseEntity<>( oldGame, HttpStatus.OK );
    }

    @DeleteMapping("/games/{name}")
    public ResponseEntity<Game> deleteGame(@RequestParam final String name) {
        Game deletedGame = gameService.deleteGame(name);
        return new ResponseEntity<>( deletedGame, HttpStatus.NO_CONTENT );
    }


}
