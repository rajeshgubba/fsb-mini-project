package com.fsb.mini.project.gameservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsb.mini.project.gameservice.cache.CacheService;
import com.fsb.mini.project.gameservice.exceptions.GameAlreadyExistsException;
import com.fsb.mini.project.gameservice.exceptions.GameNotFoundException;
import com.fsb.mini.project.gameservice.exceptions.InvalidInputException;
import com.fsb.mini.project.gameservice.model.Game;

import lombok.extern.log4j.Log4j2;

/**
 * Implementation of game service.
 *
 */

@Service
@Log4j2
public class GameServiceImpl implements GameService
{

    @Autowired
    private CacheService cacheService;

    /**
     *
     * @return
     */
    @Override
    public List<Game> listGames()
    {
        return cacheService.list();
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
    public Game findByName( final String name )
    {
        if( cacheService.get( name ).isEmpty() ) {
            throw new GameNotFoundException( "Game not found!" );
        }
        return cacheService.get( name ).get();
    }

    /**
     *
     * @param game
     */
    @Override
    public void createGame( final Game game )
    {
        if( cacheService.add( game.getName(), game ).isEmpty() ) {
            throw new GameAlreadyExistsException( "Already exists!" );
        }
    }

    /**
     *
     * @param game
     * @return
     */
    @Override
    public Game updateGame( final Game game )
    {
        if( game.getName() == null || game.getName().isBlank()) {
            throw new InvalidInputException( "Invalid game name!" );
        }

        Optional<Game> oldGame = cacheService.update( game.getName(), game );

        if(oldGame.isEmpty()) {
            throw new InvalidInputException( "Invalid game details!" );
        }

        return oldGame.get();
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
    public Game deleteGame( final String name )
    {
        Optional<Game> removedGame = cacheService.remove( name );
        if(removedGame.isEmpty()) {
            throw new InvalidInputException( "Game does not exists!" );
        }
        return removedGame.get();
    }
}
