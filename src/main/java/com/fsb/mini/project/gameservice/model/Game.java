package com.fsb.mini.project.gameservice.model;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Represents a Game object.
 *
 */
@Data
@ToString
@Builder
public class Game
{
    private String name;
    private LocalDate createdAt;
    private boolean active;
}
