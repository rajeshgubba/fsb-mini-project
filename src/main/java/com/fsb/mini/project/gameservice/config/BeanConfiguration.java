package com.fsb.mini.project.gameservice.config;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fsb.mini.project.gameservice.model.Game;

/**
 * All bean configurations.
 *
 */
@Configuration
public class BeanConfiguration
{

    @Bean
    public ConcurrentHashMap<String, Game> gameCache() {
        return new ConcurrentHashMap<>();
    }

}
