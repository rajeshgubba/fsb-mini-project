package com.fsb.mini.project.gameservice.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsb.mini.project.gameservice.model.Game;

import lombok.extern.log4j.Log4j2;

/**
 * Service class to perform all game operations.
 *
 */
@Service
@Log4j2
public class CacheService
{

    @Autowired
    private Map<String, Game> gameCache;

    public Optional<Game> add( final String key, final Game game) {
        log.debug( "Adding Game Details : " + game );
        if( !gameCache.containsKey( key.toLowerCase() ) ) {
            gameCache.put( key.toLowerCase() , game);
            return Optional.of( game );
        }
        log.debug( "Already Exists Game Details : " + game );
        return Optional.empty();
    }

    public Optional<Game> get( final String key) {
        log.debug( "Get Game Details for : " + key );
        if(gameCache.containsKey( key.toLowerCase() )) {
            return Optional.of( gameCache.get( key.toLowerCase() ) );
        }

        return Optional.empty();
    }

    public Optional<Game> update( final String key,  final Game game) {
        log.debug( "Updating Game Details : " + game );
        return Optional.ofNullable(gameCache.replace( key.toLowerCase(), game ));
    }

    public Optional<Game> remove( final String key) {
        log.debug( "Removing Game Details for : " + key );
        if(gameCache.containsKey( key.toLowerCase() )) {
            return Optional.of( gameCache.remove( key.toLowerCase() ) );
        }

        return Optional.empty();
    }

    public List<Game> list() {
        return new ArrayList<>( gameCache.values() );
    }

    public void resetCache() {
        gameCache.clear();
    }
}
