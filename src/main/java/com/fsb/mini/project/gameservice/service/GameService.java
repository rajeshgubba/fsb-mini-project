package com.fsb.mini.project.gameservice.service;

import java.util.List;

import com.fsb.mini.project.gameservice.model.Game;

/**
 * Interface for Game Service.
 *
 */

public interface GameService
{

    List<Game> listGames();

    Game findByName( String name );

    void createGame( Game game );

    Game updateGame( Game game );

    Game deleteGame( String name );
}
