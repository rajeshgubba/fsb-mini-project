package com.fsb.mini.project.gameservice.exceptions;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.extern.log4j.Log4j2;

/**
 * Please add your description here.
 *
 */
@Order( Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Log4j2
public class RestExceptionHandler
{
    @ExceptionHandler( GameAlreadyExistsException.class)
    public ResponseEntity<Object> handleCacheException( GameAlreadyExistsException ex) {
        log.error( "Failed: ", ex );
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler( InvalidInputException.class)
    public ResponseEntity<Object> handleInvalidInputException( InvalidInputException ex) {
        log.error( "Failed: ", ex );
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler( GameNotFoundException.class)
    public ResponseEntity<Object> handleGameNotFoundException( GameNotFoundException ex) {
        log.error( "Failed: ", ex );
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(Exception ex) {
        log.error( "Failed: ", ex );
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
