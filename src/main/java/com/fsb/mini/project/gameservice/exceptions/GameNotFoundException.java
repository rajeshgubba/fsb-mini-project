package com.fsb.mini.project.gameservice.exceptions;

/**
 * Please add your description here.
 *
 */
public class GameNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = -1831409184527909972L;

    public GameNotFoundException(String message) {
        super(message);
    }
}
