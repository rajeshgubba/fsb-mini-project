package com.fsb.mini.project.gameservice.cache;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fsb.mini.project.gameservice.model.Game;

/**
 * Please add your description here.
 *
 */
@SpringBootTest
public class CacheServiceTest
{
    @Autowired
    private CacheService cacheService;

    @BeforeEach
    public void setUp() {
        cacheService.resetCache();
    }

    @Test
    void testCache() {
        Game testGame = Game.builder().name( "test" ).active( true ).createdAt( LocalDate.now() ).build();
        Optional<Game> added = cacheService.add( testGame.getName(), testGame );
        Assertions.assertFalse( added.isEmpty() );

        List<Game> all = cacheService.list();
        Assertions.assertEquals( 1, all.size() );

        Optional<Game> game = cacheService.get( "test" );
        Assertions.assertFalse( game.isEmpty() );
        Assertions.assertEquals( "test", game.get().getName() );

        Optional<Game> removed = cacheService.remove( "test" );
        Assertions.assertFalse( removed.isEmpty() );

        Optional<Game> invalidRemoved = cacheService.remove( "xyz" );
        Assertions.assertTrue( invalidRemoved.isEmpty() );

    }

}
