package com.fsb.mini.project.gameservice.controller;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fsb.mini.project.gameservice.exceptions.GameNotFoundException;
import com.fsb.mini.project.gameservice.model.Game;
import com.fsb.mini.project.gameservice.service.GameService;

/**
 * Please add your description here.
 *
 */
@WebMvcTest(GameController.class)
public class GameControllerTest
{

    @MockBean
    GameService gameService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void listGames() throws Exception {
        Mockito.when(gameService.listGames()).thenReturn( List.of( Game.builder().name( "cricket" ).build()));
        mockMvc.perform( MockMvcRequestBuilders
            .get("/api/games")
            .accept( MediaType.APPLICATION_JSON))
            .andDo( MockMvcResultHandlers.print() )
            .andExpect( MockMvcResultMatchers.status().isOk() );
    }

    @Test
    void getGameExisting() throws Exception {
        Mockito.when(gameService.findByName(Mockito.anyString())).thenReturn( Game.builder().name( "cricket" ).build() );
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/games/{name}", "cricket")
                .accept( MediaType.APPLICATION_JSON))
            .andDo( MockMvcResultHandlers.print() )
            .andExpect( MockMvcResultMatchers.status().isOk() )
            .andExpect( MockMvcResultMatchers.jsonPath( "$.name" ).value( "cricket" ) );
    }

    @Test
    void getGameInvalid() throws Exception {
        Mockito.when(gameService.findByName(Mockito.anyString())).thenThrow( GameNotFoundException.class );
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/games/{name}", "abc")
                .accept( MediaType.APPLICATION_JSON))
            .andDo( MockMvcResultHandlers.print() )
            .andExpect( MockMvcResultMatchers.status().isNotFound() );
    }

}
