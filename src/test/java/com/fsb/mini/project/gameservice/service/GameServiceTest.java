package com.fsb.mini.project.gameservice.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fsb.mini.project.gameservice.exceptions.GameNotFoundException;
import com.fsb.mini.project.gameservice.exceptions.InvalidInputException;
import com.fsb.mini.project.gameservice.model.Game;

/**
 * Please add your description here.
 *
 */
@SpringBootTest
@TestMethodOrder( MethodOrderer.OrderAnnotation.class)
public class GameServiceTest
{

    @Autowired
    GameService gameService;

    @Test
    @Order(1)
    void testGetAll() {
        List<Game> list =  gameService.listGames();
        Assertions.assertEquals( 0, list.size() );
    }

    @Test
    @Order(2)
    void testFindByName_NotExists() {
        Assertions.assertThrows( GameNotFoundException.class, () -> gameService.findByName("invalid") );
    }

    @Test
    @Order(3)
    void testUpdate_NotExists() {
        Assertions.assertThrows( InvalidInputException.class, () -> gameService.updateGame(Game.builder().build()));
    }

    @Test
    @Order(4)
    void testDelete_NotExists() {
        Assertions.assertThrows( InvalidInputException.class, () -> gameService.deleteGame("invalid"));
    }

    @Test
    @Order(5)
    void testAdd() {
        gameService.createGame(Game.builder().name( "Cricket" ).build());
        Assertions.assertNotNull( gameService.findByName( "cricket" ) );
    }

}
